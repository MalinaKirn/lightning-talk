Lightning talk example of python-template
=====================================================
This repository is a simple working example of a project utilizing
[python-template](https://bitbucket.org/MalinaKirn/python-template).

The slides accompanying this demo are
[here](2017-09-06_lightning_talk.pdf).

The simple code in this package can be executed after installation by calling `say_hello`.

This package includes examples of:

1. A [setup.py](setup.py) that declares a pandas
   dependency using loose versioning (not actually used, just an example) and registers a console script (`say_hello`)
1. A [requirements.txt](requirements.txt) that declares a
   pandas dependency using strict versioning
1. Source that reads from package resources,
   [lightning/my_module/some_code.py](lightning/my_module/some_code.py)
1. A simple unit test,
   [lightning/my_module/tests/test_some_code.py](lightning/my_module/tests/test_some_code.py)
1. A unit test that loads test resources by filepath (since test resources are not included in the package),
   [lightning/tests/test_pep8.py](lightning/tests/test_pep8.py)

Installing For Use
------------------
If you'd simply like to use lightning, clone this repository, navigate into it, then execute inside your relevant
virtualenv (if you have one):

    pip install -r requirements.txt
	pip install .

Note that this will not install the source code in your environment.

Installing For Development
--------------------------
If you want to develop this package, clone this repository, navigate into it, then create and use a virtualenv for this
project:

    mkvirtualenv lightning
    pip install -r requirements.txt
    pip install -e .[tests]

This installs the `lightning` package, specific versions of its dependencies, and additional dependencies required for
tests.

The additional `-e` flag configures the virtual environment to use the source code located in the `lightning` directory,
which will cause code using the package to run against your latest changes to the source code without requiring you to
reinstall the package every time you modify the code. Note: in most Python shells, once code is imported in a session,
it's not reimported in the same session, even if you explicitly execute an import statement again. Consequently, if you
change source code and want to use it in a running IPython notebook or Python shell that has already imported the code,
you'll have to exit the notebook or shell in order to import your new source code.

Note that using `pip freeze` to create the requirements.txt file is discouraged, as this introduces dependencies that
may not be system-agnostic. However, requirements.txt should pin the versions of every package listed in the
`install_requires` section of setup.py.

Running Unit Tests
------------------
Unit tests are executed using nose. A setup.cfg is checked into the project specifying the nose configuration. Nose
tests can be run from the command line or from IntelliJ/PyCharm.

### From the command line:

    nosetests -a '!disabled'

### From IntelliJ/PyCharm:

- Create a Run/Debug Configuration using the Python tests -> Nosetests template.
- Configure the Nosetests to run "All in folder", specifying the `lightning` sub-directory as the folder.
- Set the params to: `-a '!disabled'`.
- Set the working directory to this top-level directory (one directory above the `lightning` sub-directory).

### Code Coverage:

Running nosetests with the coverage flag enabled in IntelliJ/PyCharm
[interferes with debugging](http://pydev.blogspot.com/2007/06/why-cant-pydev-debugger-work-with.html).
Consequently, the with-coverage flag is not enabled in setup.cfg. Coverage can be determined from the command line by
running `nosetests` with the `--with-coverage` flag. If using the nosetests `--with-coverage` flag, coverage reports in
HTML with line highlighting can be read from the `cover/index.html` file.

Enterprise versions of IntelliJ/PyCharm can run coverage analysis on its own, but won't fail unit tests if coverage
drops below the minimum set in setup.cfg.

API Docs
--------

If you would like the needed dependencies for generating API docs, execute:

    pip install -e .[docs]

If you wish to generate the API docs using Sphinx *including the source code* (see below), you **must** use
`pip install -e .[docs]` to install this package, as the `-e` flag installs the source code in your environment for use
by Sphinx. I.e., you cannot use `pip install .[docs]`.

The code is documented with reStructuredText syntax fed into Sphinx to create API docs. To generate API documentation:

    sphinx-apidoc -F -o apidocs lightning `find . \( -name tests -o -name resources \) -type d`
    python setup.py build_sphinx

This generates documentation for every package and module inside the lightning directory, except for directories named
tests or resources. The HTML output will be available at build/html/index.html.

Committing
----------
Fork this repository and commit changes to your fork. When ready, submit a pull request for code review.

Unit tests for new functionality are strongly encouraged. All current tests must pass or be suitably modified.

Python dosctrings for all new functions and classes is highly recommended. Use reStructuredText syntax. Type hinting
using [PyCharm's suggested type syntax](https://www.jetbrains.com/pycharm/webhelp/type-hinting-in-pycharm.html) is
recommended.

If you update the python package version number in setup.py, update the package number in apidocs/conf.py as well.
