#!/usr/bin/env python

import os
from setuptools import setup, find_packages

tests_require = ['coverage==4.4.1',
                 'mock==2.0.0',
                 'nose==1.3.7',
                 'pep8==1.7.0',
                 'testfixtures==5.2.0']

setup(name='lightning',
      version='0.1.0',
      description='Description',
      author='author',
      author_email='author@domain.com',
      classifiers=['Private :: Do Not Upload',
                   'Programming Language :: Python :: 2'],
      entry_points={
          'console_scripts': [
              'say_hello = lightning.__main__:main'
          ]
      },
      package_dir={'lightning': 'lightning'},
      package_data={'': [os.path.join('resources','*'), os.path.join('resources','.*')]},
      packages=find_packages(exclude=['tests', '*.tests', '*.tests.*']),
      install_requires=['pandas>=0.19.2,<1.0.0'],
      tests_require=tests_require,
      test_suite='nose.collector',
      extras_require={'tests': tests_require,
                      'docs': ['Sphinx==1.6.3',
                               'docutils==0.14',
                               'sphinx_py3doc_enhanced_theme==2.4.0',
                               'sphinx-pypi-upload==0.2.1']})
