import os
import unittest

import pep8


class TestPep8(unittest.TestCase):
    def test_style(self):
        pep8style = pep8.StyleGuide(config_file=os.path.join('lightning', 'tests', 'resources', 'pep8.cfg'))
        report = pep8style.init_report()
        report.start()
        pep8style.input_dir('lightning')
        report.stop()
        self.assertEqual(report.total_errors, 0,
                         "Found %i code style errors (and warnings)." % report.total_errors)
