import logging
import sys

from lightning.my_module import some_code

logger = logging.getLogger(__name__)


def main(argv=None):
    """
    Main method docs.

    :type argv: list[str]
    :param argv: the CLI args
    """
    # This is registered as the entry point for setuptools, which will call this function directly and won't pass in CLI
    # args; if this is called via the lightning executable or via python -m lightning, we need to pick up args.
    if argv is None:
        argv = sys.argv[1:]

    try:
        print some_code.hello()

    # This exception clause catches all exceptions that percolate up, logs them, then re-raises them. Exceptions are
    # not logged by default, but we want to ensure that they are, especially if we have non-console log handlers.
    except:
        e = sys.exc_info()
        # If there are no logging handlers, or the only handler is a console logger, there's no point in logging;
        # python main functions will always print exceptions to console, no need to repeat via StreamHandler.
        # Note that if a StreamHandler and any other handler is defined, this will print the exception to console twice:
        # once from the logger, and once for python main. But we do it anyway to ensure the exception shows up in the
        # *other* output handlers (for instance, a FileHandler) and to ensure the process properly exits with failure.
        root_handlers = logging.getLogger().handlers
        if root_handlers and not (len(root_handlers) == 1 and isinstance(root_handlers[0], logging.StreamHandler)):
            # The one exception we don't want logged is a SystemExit of 0, indicating normal exit
            if not (isinstance(e[1], SystemExit) and e[1].code == 0):
                # logger.exception will print the stacktrace, but let's capture the error message on the first log line
                logger.exception("%s: %s" % (e[0].__name__, e[1]))
        raise


if __name__ == "__main__":
    main(sys.argv[1:])
