from unittest import TestCase

from lightning.my_module import some_code


class TestSomeCode(TestCase):

    def test_hello(self):
        self.assertEqual(some_code.hello(), "Hello to lightning talk participants!")
