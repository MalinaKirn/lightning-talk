import os
import pkg_resources


def hello():
    """
    Reads from package resources to get the appropriate greeting

    :rtype: str
    :return: An appropriate greeting
    """
    resource_package = __name__
    resource_path = os.path.join('resources', 'hello.txt')
    return pkg_resources.resource_string(resource_package, resource_path)
